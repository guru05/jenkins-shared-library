def call(sonarqubeQualityGateStatus){
    if (sonarqubeQualityGateStatus.contains('"status":"ERROR"'))
{
    currentBuild.result = 'FAILURE'
    error "Sonarqube quality gate failed. See Sonarqube for details."
}
else {
    currentBuild.result = 'SUCCESS'
    echo "Sonarqube quality gate passed. Proceeding with the build."
}
}
