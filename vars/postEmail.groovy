def call(result) {
    mail to: "guruprashath.manickam@expleogroup.com",
    subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}",
    emailext attachLog: "true",
    compressLog: "true",
    body: "${currentBuild.currentResult}: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} ${currentBuild.getBuildCauses()[0].shortDescription} \n More info at: ${env.BUILD_URL}\n Please find the log file attached for your reference"
}

//subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}",
//   body: "${currentBuild.currentResult}: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} ${currentBuild.getBuildCauses()[0].//////////shortDescription} \n More info at: ${env.BUILD_URL}\n Please find the log file attached for your reference"
