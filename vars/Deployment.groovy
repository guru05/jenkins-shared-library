def call() {
     sh '''#!/bin/bash
               git config --global user.name "parademahesh"
               git config --global user.email "pardemahesg@gmail.com" 
               awk '{sub(/autodeployment.*/, "autodeployment:'"${BUILD_NUMBER}"'")} 1' spring-boot-app-manifests/deployment.yml > spring-boot-app-manifests/deployment.tmp && mv spring-boot-app-manifests/deployment.tmp spring-boot-app-manifests/deployment.yml
               cat spring-boot-app-manifests/deployment.yml
               git add .
               git commit -m "Update deployment image to version ${BUILD_NUMBER}"
               '''
}
