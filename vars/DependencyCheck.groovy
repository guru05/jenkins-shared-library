def call (){   
dependencyCheck additionalArguments: ''' 
-o "./" 
-s "./"
-f "ALL" 
--prettyPrint''', odcInstallation: 'dependency-check-6.5.0'
dependencyCheckPublisher pattern: 'dependency-check-report.xml'
}
